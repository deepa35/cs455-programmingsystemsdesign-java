// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA3
// Spring 2018

import java.util.LinkedList;

/**
   Maze class

   Stores information about a maze and can find a path through the maze
   (if there is one).

   Assumptions about structure of the maze, as given in mazeData, startLoc, and endLoc
   (parameters to constructor), and the path:
   -- no outer walls given in mazeData -- search assumes there is a virtual 
   border around the maze (i.e., the maze path can't go outside of the maze
   boundaries)
   -- start location for a path is maze coordinate startLoc
   -- exit location is maze coordinate exitLoc
   -- mazeData input is a 2D array of booleans, where true means there is a wall
   at that location, and false means there isn't (see public FREE / WALL 
   constants below) 
   -- in mazeData the first index indicates the row. e.g., mazeData[row][col]
   -- only travel in 4 compass directions (no diagonal paths)
   -- can't travel through walls

*/

public class Maze {
    public static final boolean FREE = false;
    public static final boolean WALL = true;

    private boolean[][] mazeLayout;
    private MazeCoord startPoint;
    private MazeCoord endPoint;
    private LinkedList<MazeCoord> path;
    private boolean searched;

    /**
     * Constructs a maze.
     * @param mazeData the maze to search.  See general Maze comments above for what
     * goes in this array.
     * @param startLoc the location in maze to start the search (not necessarily on an edge)
     * @param exitLoc the "exit" location of the maze (not necessarily on an edge)
     * PRE: 0 <= startLoc.getRow() < mazeData.length and 0 <= startLoc.getCol() < mazeData[0].length
     * and 0 <= endLoc.getRow() < mazeData.length and 0 <= endLoc.getCol() < mazeData[0].length
     * 
     * Representation Invariants:
     * The row number in the maze varies from 0 to mazeData.length-1
     * The column number in the maze varies from 0 to mazeData[0].length-1
     * Wall is represented by 'true' and space by 'false'
     */
    public Maze(boolean[][] mazeData, MazeCoord startLoc, MazeCoord exitLoc) {
	mazeLayout = mazeData.clone();
	startPoint = startLoc;
	endPoint = exitLoc;
	path = new LinkedList<MazeCoord>();
	searched = false;
    }

    /**
     * Returns the number of rows in the maze
     * @return number of rows
     */
    public int numRows() {
	return mazeLayout.length;   
    }

    /**
     * Returns the number of columns in the maze
     * @return number of columns
     */   
    public int numCols() {
	return mazeLayout[0].length;   
    } 

    /**
     * Returns true iff there is a wall at this location
     * @param loc the location in maze coordinates
     * @return whether there is a wall here
     * PRE: 0 <= loc.getRow() < numRows() and 0 <= loc.getCol() < numCols()
     */
    public boolean hasWallAt(MazeCoord loc) {
	int locRow = loc.getRow();
	int locCol = loc.getCol();
	if(mazeLayout[locRow][locCol] == Maze.WALL) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Returns the entry location of this maze.
     * @return the starting point
     */
    public MazeCoord getEntryLoc() {
	return startPoint;  
    }

    /**
     * Returns the exit location of this maze.
     * @return the exit point
     */
    public MazeCoord getExitLoc() {
	return endPoint;   
    }
	
    /**
     * Returns the path through the maze. First element is start location, and
     * last element is exit location.  If there was not path, or if this is called
     * before a call to search, returns empty list.
     * @return the maze path
     */
    public LinkedList<MazeCoord> getPath() {
	return path;
    }

    /**
     * Find a path from start location to the exit location (see Maze
     * constructor parameters, startLoc and exitLoc) if there is one.
     * Client can access the path found via getPath method.
     * @return whether a path was found.
     * 
     * PRE: rows of visited nodes varies from 0 to numRows()-1
     * columns of visited nodes varies from 0 to numColumns()-1
     * visitedNodes have value true if the point corresponding to [row,col] is already visited, otherwise false 
     */
    public boolean search() { 
	if(!searched) {
	    boolean[][] visitedNodes = new boolean[numRows()][numCols()];
	    System.out.println("DEBUG: Inside search function");	    
	    searched = true;
	    if(hasWallAt(startPoint) || hasWallAt(endPoint)) {
		return false;
	    }
	    return findPath(startPoint, visitedNodes);
	} else {
	    //if already searched return the value of path
	    if(getPath().isEmpty()) {
		return false;
	    } 
	    return true;
	}
    }

    /**
     * Recursive helper class to find a path from start to exit
     * @param start the starting point of path search
     * @param visitedNodes boolean array of visit status
     * @return whether a path is found
     */
    private boolean findPath(MazeCoord start, boolean[][] visitedNodes) {
	if(isValid(start)) {
	    if(start.equals(endPoint)) {
		path.addFirst(start);
		return true;
	    } else if(isVisited(start,visitedNodes) || hasWallAt(start)) {
		return false;
	    } else {
		setVisited(start,visitedNodes);
		if(findPath(new MazeCoord(start.getRow()-1, start.getCol()), visitedNodes) ||
		   findPath(new MazeCoord(start.getRow(), start.getCol()-1), visitedNodes) ||
		   findPath(new MazeCoord(start.getRow()+1, start.getCol()), visitedNodes) ||
		   findPath(new MazeCoord(start.getRow(), start.getCol()+1), visitedNodes)){
		    path.addFirst(start);
		    return true;
		}
	    }
	    return false;
	}else {
	    return false;
	}
    }

    /**
     * Marks a location as visited
     * @param location element which has to be marked
     * @param visitedNodes boolean array of visit status
     */
    private void setVisited(MazeCoord location, boolean[][] visitedNodes) {
	visitedNodes[location.getRow()][location.getCol()] = true;
    }

    /**
     * Verifies whether a location has already been visited or not during search
     * @param location the location to be verified
     * @param visitedNodes boolean array of visit status
     * @return whether the location is visited or not
     */
    private boolean isVisited(MazeCoord location, boolean[][] visitedNodes) {
	return visitedNodes[location.getRow()][location.getCol()];
    }

    /**
     * Verifies whether a location is inside a maze or not
     * @param element the location which is to be verified
     * @return whether element is a part of the maze or not
     */
    private boolean isValid(MazeCoord element) {
	if(element.getRow() < 0 || element.getRow() >= this.numRows() ||element.getCol() < 0 || element.getCol() >= this.numCols()) {
	    return false;
	} 
	return true;
    }
}
