// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA3
// Spring 2018


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFrame;

/**
 * MazeViewer class
 * 
 * Program to read in and display a maze and a path through the maze. At user
 * command displays a path through the maze if there is one.
 * 
 * How to call it from the command line:
 * 
 *      java MazeViewer mazeFile
 * 
 * where mazeFile is a text file of the maze. The format is the number of rows
 * and number of columns, followed by one line per row, followed by the start location, 
 * and ending with the exit location. Each maze location is
 * either a wall (1) or free (0). Here is an example of contents of a file for
 * a 3x4 maze, with start location as the top left, and exit location as the bottom right
 * (we count locations from 0, similar to Java arrays):
 * 
 * 3 4 
 * 0111
 * 0000
 * 1110
 * 0 0
 * 2 3
 * 
 */

public class MazeViewer {
    private static final char WALL_CHAR = '1';
    private static final char FREE_CHAR = '0';

    public static void main(String[] args)  {
	String fileName = "";
	try {
	    if (args.length < 1) {
		System.out.println("ERROR: missing file name command line argument");
	    }
	    else {
		fileName = args[0];
		JFrame frame = readMazeFile(fileName);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	    }
	}
	catch (FileNotFoundException exc) {
	    System.out.println("ERROR: File not found: " + fileName);
	}
	catch (IOException exc) {
	    exc.printStackTrace();
	}
    }

    /**
     * readMazeFile reads in maze from the file whose name is given and 
     * returns a MazeFrame created from it.
     * @param fileName the name of a file to read from (file format shown in class comments, above)
     * @returns a MazeFrame containing the data from the file
     * @throws FileNotFoundException if there's no such file (subclass of IOException)
     * @throws IOException (hook given in case you want to do more error-checking --
     * that would also involve changing main to catch other exceptions)
     */
    private static MazeFrame readMazeFile(String fileName) throws IOException {
	File mazeData = new File(fileName);
	int rowSize = 0, columnSize = 0;
	MazeCoord startLoc, endLoc;

	Scanner fileReader = new Scanner(mazeData);
	Scanner lineReader = null;

	if(fileReader.hasNextLine()) {
	    lineReader = new Scanner(fileReader.nextLine());
	    rowSize = lineReader.nextInt();
	    columnSize = lineReader.nextInt();
	}

	boolean[][] mazeStructure = new boolean[rowSize][columnSize];

	readMazeStructure(fileReader, mazeStructure);
		
	startLoc = readLocation(fileReader, lineReader);
	endLoc = readLocation(fileReader, lineReader);
		
	fileReader.close();
	lineReader.close();
		
	return new MazeFrame(mazeStructure, startLoc, endLoc);
    }

    /**
     * Reads the start and end point of the maze from file whose name is given
     * @param fileReader scanner object that reads the file
     * @param lineReader scanner object that reads each line of the file
     * @return the MazeCoord corresponding to entry/exit location
     */
    private static MazeCoord readLocation(Scanner fileReader, Scanner lineReader) {
	int row = 0;
	int column = 0;
	if(fileReader.hasNextLine()) {
	    lineReader = new Scanner(fileReader.nextLine());
	    row = lineReader.nextInt();
	    column = lineReader.nextInt();
	}
	return new MazeCoord(row,column);
    }

    /**
     * Reads the maze data from the file whose name is given
     * @param fileReader fileReader scanner object that reads the file
     * @param mazeStructure lineReader scanner object that reads each line of the file
     */
    private static void readMazeStructure(Scanner fileReader, boolean[][] mazeStructure) {
	for(int row = 0; row<mazeStructure.length; row++) {
	    if(fileReader.hasNextLine()) {
		String rowData = fileReader.nextLine();
		for(int column = 0; column<mazeStructure[0].length; column++) {
		    if(rowData.charAt(column) == WALL_CHAR) {
			mazeStructure[row][column] = Maze.WALL;
		    } else if(rowData.charAt(column) == FREE_CHAR) {
			mazeStructure[row][column] = Maze.FREE;
		    }
		}
	    }
	}
    }
}
