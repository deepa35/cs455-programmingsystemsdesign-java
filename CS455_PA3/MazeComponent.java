// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA3
// Spring 2018

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JComponent;

/**
   MazeComponent class

   A component that displays the maze and path through it if one has been found.
*/
public class MazeComponent extends JComponent
{
    // Top left of corner of maze in frame
    private static final int START_X = 10; 
    private static final int START_Y = 10;
	
    // Width and height of one maze "location"
    private static final int BOX_WIDTH = 20;  
    private static final int BOX_HEIGHT = 20;
	
    // How much smaller on each side to make entry/exit inner box
    private static final int INSET = 2;  
	
    // Colors
    private static final Color BORDER = Color.BLACK;
    private static final Color WALL_COLOR = Color.BLACK;
    private static final Color FREE_COLOR = Color.WHITE;
    private static final Color STARTLOC_COLOR = Color.YELLOW;
    private static final Color ENDLOC_COLOR = Color.GREEN;
    private static final Color PATH_COLOR = Color.BLUE;

    private Maze myMaze;

    /**
       Constructs the component.
       @param maze   the maze to display
    */
    public MazeComponent(Maze maze) {   
	myMaze = maze;
    }

    /**
       Draws the current state of maze including the path through it if one has
       been found.
       @param g the graphics context
    */
    public void paintComponent(Graphics g) {
	Graphics2D g2D = (Graphics2D) g;
		
	//drawing the maze with entry/exit points
	drawMaze(g2D);
		
	//Setting the border
	g2D.setColor(BORDER); 
	g2D.drawRect(START_X, START_Y, BOX_WIDTH * myMaze.numCols(), BOX_HEIGHT * myMaze.numRows());
		
	//drawing the path
	drawPath(g2D);
    }

    /**
     * Draws the path found through the maze if one has been found
     * @param g2D the 2D graphics context
     */
    private void drawPath(Graphics2D g2D) {
	LinkedList<MazeCoord> myPath = myMaze.getPath();
	MazeCoord start = null, end = null;
	if(!myPath.isEmpty()) {
	    ListIterator<MazeCoord> pathFinder = myPath.listIterator();
	    g2D.setColor(PATH_COLOR);
	    if(pathFinder.hasNext()) {
		start = pathFinder.next();
		//Debug point
		System.out.println("DEBUG: " + start); 
	    }
	    for(int numLines = 0; numLines<myPath.size()-1; numLines++) {
		if(pathFinder.hasNext()) {
		    end = pathFinder.next();
		    //Debug point
		    System.out.println("DEBUG: " + end); 
		}
		int xStart = START_X + (start.getCol()* BOX_WIDTH) + (BOX_WIDTH /2);
		int yStart = START_Y + (start.getRow()* BOX_HEIGHT) + (BOX_HEIGHT/2);
		int xEnd = START_X + (end.getCol()* BOX_WIDTH) + (BOX_WIDTH /2);
		int yEnd = START_Y + (end.getRow()* BOX_HEIGHT) + (BOX_HEIGHT/2);
		g2D.drawLine(xStart, yStart, xEnd, yEnd);
		start = end;
	    }
	}
    }

    /**
     * Draws the complete maze
     * @param g2D the 2D graphics context
     */
    private void drawMaze(Graphics2D g2D) {
	for(int row = 0; row<myMaze.numRows(); row++) {
	    for(int column = 0; column<myMaze.numCols(); column ++) {
		MazeCoord loc = new MazeCoord(row,column);
		drawWallFree(g2D, loc);
				
		if(loc.equals(myMaze.getEntryLoc()) || loc.equals(myMaze.getExitLoc())) {
		    drawEntryExitPoint(g2D, loc);
		}
	    }
	}
    }

    /**
     * Draws each element of a maze depending on whether the element
     * is a free space or a wall
     * @param g2D the 2D graphics context
     * @param loc the element where a free space or wall is to be constructed
     */
    private void drawWallFree(Graphics2D g2D, MazeCoord loc) {
	if(myMaze.hasWallAt(loc)) {
	    g2D.setColor(WALL_COLOR);
	} else {
	    g2D.setColor(FREE_COLOR); 
	}
	g2D.fillRect(START_X + (loc.getCol() *BOX_WIDTH), START_Y + (loc.getRow() * BOX_HEIGHT), BOX_WIDTH, BOX_HEIGHT);
    }

    /**
     * Draws the entry/exit element
     * @param g2D the 2D graphics context
     * @param loc the entry/exit element
     */
    private void drawEntryExitPoint(Graphics2D g2D, MazeCoord loc) {
	if(loc.equals(myMaze.getEntryLoc())) {
	    g2D.setColor(STARTLOC_COLOR);
	} else {
	    g2D.setColor(ENDLOC_COLOR);
	}
	g2D.fillRect(START_X + (loc.getCol() *BOX_WIDTH) + INSET, START_Y + (loc.getRow() * BOX_HEIGHT) + INSET, BOX_WIDTH - (2*INSET), BOX_HEIGHT - (2*INSET));
    }
}
