// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CSCI455 PA2
// Spring 2018

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * class SolitaireBoard
 * 
 * The board for Bulgarian Solitaire.
 * Includes methods for solitaire board creation and playing the rounds
 * Total number of cards for the game can be changed by changing NUM_FINAL_PILES.
 */
public class SolitaireBoard {

    public static final int NUM_FINAL_PILES = 9;
    // number of piles in a final configuration
    // (note: if NUM_FINAL_PILES is 9, then CARD_TOTAL below will be 45)

    public static final int CARD_TOTAL = NUM_FINAL_PILES * (NUM_FINAL_PILES + 1) / 2;
    // bulgarian solitaire only terminates if CARD_TOTAL is a triangular number.
    // the above formula is the closed form for 1 + 2 + 3 + . . . + NUM_FINAL_PILES

    /**
     * Representation invariant: 
     * For NUM_FINAL_PILES = 9, we have CARD_TOTAL = 45
     * CARD_TOTAL = 1+2+3+...+NUM_FINAL_PILES
     * pileLength varies from 1 to CARD_TOTAL and contains the number of non-zero elements in piles array 
     * piles array indices range from 0 to CARD_TOTAL-1 
     * Sum of all the elements in piles array should be CARD_TOTAL
     * piles array element should not be less than or equal to 0 
     */

    private int[] piles;
    private int pileLength;
    private Random pileCardCountGenerator;

    /**
     * Creates a solitaire board with the configuration specified in piles.
     * 
     * @param piles has the number of cards in the first pile, then the number of cards in the second pile, etc.
     * PRE: piles contains a sequence of positive numbers that sum to SolitaireBoard.CARD_TOTAL
     */
    public SolitaireBoard(ArrayList<Integer> piles) {
	this.piles = new int[CARD_TOTAL]; 
	pileLength = 0;
	for(int eachPile = 0; eachPile<piles.size(); eachPile++) {
	    this.piles[eachPile] = piles.get(eachPile);
	    pileLength++;
	}
	assert isValidSolitaireBoard(); 
    }

    /**
     * Creates a solitaire board with a random initial configuration.
     * 
     * PRE: piles contains a sequence of positive numbers that sum to SolitaireBoard.CARD_TOTAL
     */
    public SolitaireBoard() {
	piles = new int[CARD_TOTAL];
	pileLength = 0;
	pileCardCountGenerator = new Random();
	generateRandomPile();
	assert isValidSolitaireBoard();
    }

    /**
     * Plays one round of Bulgarian solitaire.
     * 
     * Updates the configuration according to the rules of Bulgarian solitaire.
     * Takes one card from each pile and puts them all together in a new pile. The old piles that
     * are left will be in the same relative order as before and the new pile will be at the end.
     */
    public void playRound() {
	for(int pileIndex = 0; pileIndex<pileLength; pileIndex++) {
	    piles[pileIndex]--;
	    //Decreases each pile by 1
	}
	int sizeOfNewPile = pileLength;
	checkAndDeleteZeros();
	//Deletes empty piles if any
	piles[pileLength] = sizeOfNewPile;
	//Add a new pile with size pileLength
	pileLength++;
		
	assert isValidSolitaireBoard();
    }

    /**
     * Returns true iff the current board is at the end of the game.
     * 
     * There are NUM_FINAL_PILES piles that are of sizes 1, 2, 3, . . . , NUM_FINAL_PILES in any order.
     */
    public boolean isDone() {
	if(pileLength != NUM_FINAL_PILES) {
	    return false;
	}

	//Checking for duplicate values
	boolean[] dupeChecker = new boolean[CARD_TOTAL];
	Arrays.fill(dupeChecker, false);
	for(int pileIndex = 0; pileIndex<pileLength; pileIndex++) {
	    if(dupeChecker[piles[pileIndex]]) {
		return false;
	    }
	    dupeChecker[piles[pileIndex]] = true;
	}

	assert isValidSolitaireBoard();
	return true;
    }


    /**
     * Returns current board configuration as a string with the format of 
     * a space-separated list of numbers with no leading or trailing spaces.
     * The numbers represent the number of cards in each non-empty pile.
     */
    public String configString() {
	String boardConfig = "";
	for(int eachPile = 0; eachPile < pileLength; eachPile++) {
	    if(eachPile != 0) {
		boardConfig = boardConfig + " ";
	    }
	    boardConfig = boardConfig + piles[eachPile];
	}

	assert isValidSolitaireBoard();
	return boardConfig;   
    }


    /**
     * Returns true iff the solitaire board data is in a valid state
     * (See representation invariant comment for more details.)
     */
    private boolean isValidSolitaireBoard() {
	int pileSizeSum = 0;
	if(pileLength > CARD_TOTAL || pileLength < 1) {
	    //Checking limits of pileLength
	    return false;
	}
	for(int eachPile=0; eachPile<pileLength;eachPile++) {
	    if(piles[eachPile]<=0) {
		return false;
	    }
	    pileSizeSum = pileSizeSum + piles[eachPile];
	}
	if (pileSizeSum == CARD_TOTAL) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Deletes the empty piles in the piles array, if any exists and
     * reduces the pileLength accordingly.
     */
    private void checkAndDeleteZeros() {
	int indexCounter = 0;
	int sizeCopy = pileLength;
	for (int originalIndex = 0; originalIndex < pileLength; originalIndex++) {
	    if (piles[originalIndex] != 0) {
		piles[indexCounter++] = piles[originalIndex]; 
		//indexCounter will be incremented along with originalIndex iff the pile is non zero
	    } else {
		sizeCopy--;
	    }
	}
	while (indexCounter < pileLength) {
	    piles[indexCounter++] = 0;
	    //Changing the last elements to zero (Not necessary because we adjust pileLength)
	}
	pileLength = sizeCopy;
	//Adjusting the array size
    }

    /**
     * Generates a random pile such that the number of cards in each pile add to CARD_TOTAL
     * Called only for default SolitaireBoard constructor
     */
    private void generateRandomPile() {
	int sum = 0;
	while(sum<CARD_TOTAL) {
	    int tempPileSize = pileCardCountGenerator.nextInt(CARD_TOTAL - sum) + 1;
	    //Generating a random pile size
	    //Adding 1 to make sure size never becomes 0 and to make pileSize vary from 1 to CARD_TOTAL
	    piles[pileLength] = tempPileSize;
	    sum = sum + tempPileSize;
	    pileLength++;
	}
    }
}
