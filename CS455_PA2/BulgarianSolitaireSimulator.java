// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CSCI455 PA2
// Spring 2018

import java.util.ArrayList;
import java.util.Scanner;

/**
 * class BulgarianSolitaireSimulator
 * 
 * Simulates a Bulgarian Solitaire game and outputs the card count in each pile
 * for every round. The simulator stops when the final configuration is achieved.
 * 
 * Simulator has normal mode of operation where it generates a random configuration.
 * -u command line argument prompts for initial configuration from users instead of
 * generating random configuration.
 * 
 * -s command line argument stops the program in between every rounds of the game.
 * The game continues only if the user presses enter.
 * 
 * Using NUM_PILES = 9 and CARD_TOTAL = 45. 
 * Final configuration pile size is 1,2,3,4,5,6,7,8,9 in some order.
 */

public class BulgarianSolitaireSimulator {

    public static void main(String[] args) {
	boolean singleStep = false;
	boolean userConfig = false;
	Scanner reader = new Scanner(System.in);
	SolitaireBoard myBoard;

	for (int i = 0; i < args.length; i++) {
	    if (args[i].equals("-u")) {
		userConfig = true;
	    }
	    else if (args[i].equals("-s")) {
		singleStep = true;
	    }
	}

	if (userConfig) {
	    ArrayList<Integer> cardPile = new ArrayList<Integer>(); 
	    inputUserElements(reader, cardPile);
	    myBoard = new SolitaireBoard(cardPile);
			
	} else {
	    myBoard = new SolitaireBoard();
	}
	System.out.println("Initial configuration: " + myBoard.configString());
	playGameRound(myBoard, singleStep, reader);
    }

    /**
     * Plays the entire game by removing one card from each pile and forming a new pile.
     * The method checks for final configuration status before playing the next round. 
     * 
     * @param myBoard The current SolitaireBoard instance.
     * @param singleStep Status of whether the program should stop in between each round.
     * @param reader Scanner instance used to read/detect user inputs.
     */
    private static void playGameRound(SolitaireBoard myBoard, boolean singleStep, Scanner reader) {
	int roundNumber = 0;
	while(!myBoard.isDone()) {
	    myBoard.playRound();
	    roundNumber++;
	    System.out.println("["+ roundNumber +"] Current configuration: "+ myBoard.configString());	
	    if(singleStep) { 
		System.out.print("<Type return to continue>");
		if(!myBoard.isDone()) {
		    reader.nextLine();
		} else{
		    System.out.println();
		}
	    }
	}
	System.out.println("Done!");
    }

    /**
     * Creates an arraylist of initial card configuration entered by user after validation.
     * (Executed only if userConfig is enabled)
     * 
     * @param reader Scanner instance used for reading the initial card configuration.
     * @return The initial valid card configuration.
     */
    private static void inputUserElements(Scanner reader, ArrayList<Integer> cardPile) {
	boolean validationStatus;
	System.out.println("Number of total cards is " + SolitaireBoard.CARD_TOTAL);
	System.out.println(
			   "You will be entering the initial configuration of the cards (i.e., how many in each pile).\nPlease enter a space-separated list of positive integers followed by newline:");
	do {
	    validationStatus = validateAndAddElements(reader,cardPile);
	    if(validationStatus == false) {
		cardPile.clear();
		System.out.println("ERROR: Each pile must have at least one card and the total number of cards must be "+ SolitaireBoard.CARD_TOTAL+ "\nPlease enter a space-separated list of positive integers followed by newline:");
	    }
	} while(!validationStatus);
    }

    /**
     * Validates the card configuration entered by user. Returns false if any value entered by the user
     * is not an integer or less than/equal to zero. Also, validates that the sum of pile size is equal to CARD_TOTAL.
     * 
     * @param reader Scanner instance used to read/detect user inputs.
     * @param cardPile Initial unvalidated card configuration entered by user.
     * @return Status of whether the card configuration is valid.
     */
    private static boolean validateAndAddElements(Scanner reader, ArrayList<Integer> cardPile) {
	Scanner lineReader = new Scanner(reader.nextLine());
	int pileSizeSum = 0;

	while (lineReader.hasNext()) {
	    //Checking if every element contains any character other than digits 0-9 
	    if(!lineReader.hasNextInt()) {
		lineReader.close();
		return false;
	    }
	    //Enters iff elementString contains only integers
	    int elementInteger = lineReader.nextInt();
	    if(elementInteger>0) {
		pileSizeSum = pileSizeSum + elementInteger;
		//Sum of sizes of card pile
		cardPile.add(elementInteger);
	    } else {
		lineReader.close();
		return false;
	    }
	} 

	lineReader.close();
	//Checking if sum of values entered is CARD_TOTAL
	if(pileSizeSum == SolitaireBoard.CARD_TOTAL) {
	    return true;
	} else {
	    return false;
	}
    }
}
