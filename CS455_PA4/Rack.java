// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA4
// Spring 2018

import java.util.ArrayList;

/**
 * A Rack of Scrabble tiles.
 * Stored in the form of unique letter representation of the word sorted alphabetically
 * and the multiplicity of each letter in the word.
 */
public class Rack {
    private String uniqueWord;
    private int[] wordMultiplicity;

    /**
     * Initializes the rack using the word given by user.
     * 
     * @param word Word specified by user
     */
    public Rack(String word) {
	//Preprocessed word sorted by character
	wordInitialiser(word);
    }
	
    /**
     * Returns subset of all possible words of varying length
     * that can be formed out of the given word.
     * 
     * @return subset of possible words that can be formed
     */
    public ArrayList<String> generateSubset() {
	return new ArrayList<String>(allSubsets(uniqueWord, wordMultiplicity, 0));
    }
	
    /**
     * Formats the given word into a word of unique letters and 
     * calculates the multiplicity of each letter in the word.
     * 
     * @param word Word specified by the user
     */
    private void wordInitialiser(String word) {
	char[] charRep = word.toCharArray();
		
	//Computing unique words and multiplicity of letters in the word
	if(charRep.length > 0) {	
	    uniqueWord = Character.toString(charRep[0]);
	    wordMultiplicity = new int[word.length()];
	    wordMultiplicity[0] = 1;
	    int indexCounter = 0;

	    for(int index = 1; index<charRep.length; index++) {
		if(charRep[index] == charRep[index-1]) {
		    wordMultiplicity[indexCounter] ++;
		} else {
		    indexCounter++;
		    wordMultiplicity[indexCounter]++;
		    uniqueWord = uniqueWord + charRep[index];
		}
	    }
	} else {
	    uniqueWord = "";
	}
    }

    /**
     * Finds all subsets of the multiset starting at position k in unique and mult.
     * unique and mult describe a multiset such that mult[i] is the multiplicity of the char
     *      unique.charAt(i).
     * PRE: mult.length must be at least as big as unique.length()
     *      0 <= k <= unique.length()
     * @param unique a string of unique letters
     * @param mult the multiplicity of each letter from unique.  
     * @param k the smallest index of unique and mult to consider.
     * @return all subsets of the indicated multiset
     * @author Claire Bono
     */
    private static ArrayList<String> allSubsets(String unique, int[] mult, int k) {
	ArrayList<String> allCombos = new ArrayList<>();
	if (k == unique.length()) {  // multiset is empty
	    allCombos.add("");
	    return allCombos;
	}

	// get all subsets of the multiset without the first unique char
	ArrayList<String> restCombos = allSubsets(unique, mult, k+1);

	// prepend all possible numbers of the first char (i.e., the one at position k) 
	// to the front of each string in restCombos.  Suppose that char is 'a'...
	String firstPart = "";          // in outer loop firstPart takes on the values: "", "a", "aa", ...
	for (int n = 0; n <= mult[k]; n++) {   
	    for (int i = 0; i < restCombos.size(); i++) {  // for each of the subsets 
		// we found in the recursive call
		// create and add a new string with n 'a's in front of that subset
		allCombos.add(firstPart + restCombos.get(i));  
	    }
	    firstPart += unique.charAt(k);  // append another instance of 'a' to the first part
	}
	return allCombos;
    }
}
