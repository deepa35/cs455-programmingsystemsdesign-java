// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA4
// Spring 2018

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;

/**
 * A dictionary of all anagram sets. 
 * Note: the processing is case-sensitive; so if the dictionary has all lower
 * case words, you will likely want any string you test to have all lower case
 * letters too, and likewise if the dictionary words are all upper case.
 */
public class AnagramDictionary {
    private Map<String, ArrayList<String>> dictionary;

    /**
     * Create an anagram dictionary from the list of words given in the file
     * indicated by fileName.  
     * PRE: The strings in the file are unique.
     * 
     * @param fileName  the name of the file to read from
     * @throws FileNotFoundException  if the file is not found
     */
    public AnagramDictionary(String fileName) throws FileNotFoundException {
	dictionary = new HashMap<>();
	File dictFile = new File(fileName);
	try (Scanner dictReader = new Scanner(dictFile) ){
	    ArrayList<String> dictAnagram;
	    while(dictReader.hasNext()) {
		String word = dictReader.next();
		String wordCanonical = convertToCanonical(word);
				
		//If the dictionary doesn't contain the word, creating anagram arraylist
		if(!dictionary.containsKey(wordCanonical)) {
		    dictAnagram = new ArrayList<>();
		    dictAnagram.add(word);
		    dictionary.put(wordCanonical,dictAnagram);
		} else {
		    //Accessing the anagram arraylist and updating value
		    dictAnagram = dictionary.get(wordCanonical);
		    dictAnagram.add(word);
		    dictionary.put(wordCanonical,dictAnagram);
		}
	    }
	}
    }

    /**
     * Get all anagrams of the given string. This method is case-sensitive.
     * E.g. "CARE" and "race" would not be recognized as anagrams.
     * 
     * @param s string to process
     * @return a list of the anagrams of s
     */
    public ArrayList<String> getAnagramsOf(String word) {
	String canonKey = convertToCanonical(word);
	//Returning arraylist of anagrams
	if(dictionary.containsKey(canonKey)) {
	    return new ArrayList<String>(dictionary.get(canonKey));
	}
	return new ArrayList<String>(); 
    }

    /**
     * Converts a string to canonical form where the word is sorted
     * by its characters alphabetically.
     * 
     * @param word
     * @return canonical form of the word
     */
    private String convertToCanonical(String word) {
	char[] wordArray = word.toCharArray();
	Arrays.sort(wordArray);
	return new String(wordArray);
    }
}
