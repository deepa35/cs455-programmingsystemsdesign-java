// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA4
// Spring 2018

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Generates the list of scrabble words with their scores.
 * Creates a scrabble rack and saves the word given by user.
 * Reads in the default/specified dictionary and generates 
 * possible words and their scores.
 * Continues to give scrabble words until user inputs '.'
 * 
 */
public class WordFinder {
    private static final String DEFAULT_DICT = "sowpods.txt";

    /**
     * Creates the dictionary and plays the game.
     * 
     * @param args User defined dictionary, if exists.
     */
    public static void main(String[] args) {
	String dictName = DEFAULT_DICT;
	AnagramDictionary myDictionary;
	boolean repeat = true;
	try(Scanner in = new Scanner(System.in)) {
	    if(args.length != 0) {
		dictName = args[0];
	    }
	    myDictionary = new AnagramDictionary(dictName);
	    System.out.println("Type . to quit.");
	    while(repeat) {
		System.out.print("Rack? ");
		String myWord = in.nextLine();
		if(myWord.equals(".")) {
		    repeat = false;
		} else {
		    //Sorting the word by characters
		    char[] myWordArray = myWord.toCharArray();
		    Arrays.sort(myWordArray);
		    //String myWordSorted = Arrays.toString(myWordArray);
		    playScrabble(new String(myWordArray),myDictionary);
		}
	    }
	} catch (FileNotFoundException e) {
	    System.out.println("ERROR: Dictionary file '"+ dictName + "' does not exist.");
	    System.out.println("Exiting program.");
	}
    }
	
    /**
     * Plays the game of scrabble using the word inputted and the dictionary specified.
     * 
     * @param myWord Word inputted by user
     * @param myDict The dictionary used to generate words
     */
    private static void playScrabble(String myWord, AnagramDictionary myDict) {
	//Rack created after sorting the word characters
	Rack myRack = new Rack(myWord);
	ArrayList<String> rackSubset = myRack.generateSubset();
	ArrayList<String> scrabbleWordList = new ArrayList<>();
		
	//For each word in the rack subset, selecting the anagrams from dictionary
	for(String subWord: rackSubset) {
	    scrabbleWordList.addAll(myDict.getAnagramsOf(subWord));
	}

	System.out.println("We can make "+ scrabbleWordList.size()+" words from \""+ myWord +"\"");

	if(!scrabbleWordList.isEmpty()) {
	    scoreComputerAndPrinter(scrabbleWordList);
	}
    }
	
    /**
     * Computes the scores of words in final word list and prints them.
     * The scores are sorted in descending order and words alphabetically, 
     * if they have same score.
     * 
     * @param scrabbleWordList The final word list
     */
    private static void scoreComputerAndPrinter(ArrayList<String> scrabbleWordList) {
	Map<String,Integer> wordScoreMap = new TreeMap<>();
	ScoreTable scores = new ScoreTable();

	//Putting each word and its score into the tree
	for(String word: scrabbleWordList) {
	    int score = scores.getScore(word);
	    wordScoreMap.put(word, score);
	}
		
	//Sorting the treemap by value in descending order
	ArrayList<Map.Entry<String, Integer>> wordScoreAL = new ArrayList<>(wordScoreMap.entrySet());
	Collections.sort(wordScoreAL, new Comparator<Map.Entry<String, Integer>>() {
	    public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
		return o2.getValue().compareTo(o1.getValue());
	    }
	});
		
	System.out.println("All of the words with their scores (sorted by score):");
	for(Map.Entry<String, Integer> curr : wordScoreAL) {
	    System.out.println(curr.getValue() + ": " + curr.getKey());
	}
    }
}
