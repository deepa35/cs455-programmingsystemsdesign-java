// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA4
// Spring 2018

/**
 * ScoreTable holds the score details of all letters in the 
 * alphabet and computes the score of a given word.
 *
 */
public class ScoreTable {
    private static final int ALPHABET_COUNT = 26;
    private static final char REFERENCE_LETTER = 'a';
    private int[] scoreTable;

    /**
     * Constructs a score table that assigns score to each 
     * letter of the alphabet.
     *
     */
    public ScoreTable() {
	scoreTable = new int[ALPHABET_COUNT];
	for (int index = 0; index < ALPHABET_COUNT; index++) {
	    if (index == 0 || index == 4 || index == 8 || index == 11 || index == 13 || index == 14 || index == 17
		|| index == 18 || index == 19 || index == 20) {
		scoreTable[index] = 1;
	    } else if(index == 3 || index == 6) {
		scoreTable[index] = 2;
	    } else if(index == 1 || index == 2 || index == 12 || index == 15) {
		scoreTable[index] = 3;
	    } else if(index == 5 || index == 7 || index == 21 || index == 22 || index == 24) {
		scoreTable[index] = 4;
	    } else if(index == 10) {
		scoreTable[index] = 5;
	    } else if(index == 9 || index == 23) {
		scoreTable[index] = 8;
	    } else if(index == 16 || index == 25) {
		scoreTable[index] = 10;
	    }
	}
    }
	
    /**
     * Calculates the scrabble score of a given word.
     * 
     * @param word Scrabble word
     * @return score
     */
    public int getScore(String word) {
	int totalScore = 0;
	//Using 'a' as reference, index = letter - 'a'
	for(int index = 0; index<word.length(); index++) {
	    totalScore = totalScore + scoreTable[Character.toLowerCase(word.charAt(index)) -REFERENCE_LETTER ];
	}
	return totalScore;
    }
}
