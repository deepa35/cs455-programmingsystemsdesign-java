//Name: Deepa Sreekumar
//USC NetID: dsreekum
//CS 455 PA1
//Spring 2018

import java.util.Random;

/**
 * class CoinTossSimulator
 * 
 * Simulates trials of repeatedly tossing two coins and allows the user to
 * access the cumulative results.
 * 
 * Invariant: getNumTrials() = getTwoHeads() + getTwoTails() + getHeadTails()
 * 
 */
public class CoinTossSimulator {
	private int numberOfTrials;
	private int countTwoHeads;
	private int countHeadTail;
	private int countTwoTails;
	
	/**
	 * Creates a coin toss simulator with no trials done yet.
	 * 
	 * @param numberOfTrials number of trials to be executed for simulation; must be >=1.
	 * @param countTwoHeads count of trials with two head tosses.
	 * @param countHeadTail count of trials with one head and one tail tosses.
	 * @param countTwoTails count of trials with two tail tosses.
	 */	
	public CoinTossSimulator() {
		numberOfTrials = 0;
		countTwoHeads = 0;
		countHeadTail = 0;
		countTwoTails = 0;
	}

	/**
	 * Runs the simulation for numTrials more trials. Multiple calls to this
	 * method without a reset() between them *add* these trials to the current
	 * simulation.
	 * 
	 * @param numTrials number of trials to be executed for simulation; must be >= 1
	 */
	public void run(int numTrials) {
		numberOfTrials = numTrials;
		
		for (int eachToss = 0; eachToss < numTrials; eachToss ++) {
			Random coinFlip = new Random();	//similar to a coin
			int coinFaceToss1 = coinFlip.nextInt(2);
			int coinFaceToss2 = coinFlip.nextInt(2);
			
			//Using 0 for Head and 1 for Tail
			
			if(coinFaceToss1 == 0 && coinFaceToss2 == 0) {	//Both coin faces show Head
				countTwoHeads++;
			} else if(coinFaceToss1 == 1 && coinFaceToss2 == 1) { //Both coin faces show Tail
				countTwoTails++;
			} else {	//One Head one Tail cases
				countHeadTail++;
			}
		}
	}

	/**
	 * Get number of trials performed since last reset.
	 * @return total number of trials performed.
	 */
	public int getNumTrials() {
		return getTwoHeads() + getTwoTails() + getHeadTails(); 
	}

	/**
	 * Get number of trials that came up two heads since last reset.
	 * @return countTwoHeads total number of 2 head tosses.
	 */
	public int getTwoHeads() {
		return countTwoHeads; 
	}

	/**
	 * Get number of trials that came up two tails since last reset.
	 * @return countTwoTails total number of 2 tail tosses.
	 */
	public int getTwoTails() {
		return countTwoTails; 
	}

	/**
	 * Get number of trials that came up one head and one tail since last reset.
	 * @return countHeadTail total number of 1 head 1 tail tosses.
	 */
	public int getHeadTails() {
		return countHeadTail; 
	}

	/**
	 * Resets the simulation, so that subsequent runs start from 0 trials done.
	 */
	public void reset() {
		numberOfTrials = 0;
		countTwoHeads = 0;
		countHeadTail = 0;
		countTwoTails = 0;
	}
}
