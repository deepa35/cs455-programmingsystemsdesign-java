//Name: Deepa Sreekumar
//USC NetID: dsreekum
//CS 455 PA1
//Spring 2018

/**
 * 
 * class CoinTossSimulatorTester
 * 
 * Performs unit testing of CoinTossSimulator class
 * Previous conditions: Input should be greater than zero
 */
public class CoinTossSimulatorTester {
	public static void main(String[] args) {
		CoinTossSimulator tossSimulator = new CoinTossSimulator();
		int expectedTrial = 0;

		// TestCase 1 - Testing the constructor
		System.out.println("After constructor:");
		System.out.println("Number of trials [exp:0]: " + tossSimulator.getNumTrials());
		System.out.println("Two-head tosses: " + tossSimulator.getTwoHeads());
		System.out.println("Two-tail tosses: " + tossSimulator.getTwoTails());
		System.out.println("One-head one-tail tosses: " + tossSimulator.getHeadTails());
		System.out.println("Tosses add up correctly? "
				+ (tossSimulator.getNumTrials() == tossSimulator.getTwoHeads() + tossSimulator.getTwoTails() + tossSimulator.getHeadTails())
				+ "\n");

		// TestCase 2,3,4 - Testing the run method; trials = 1,10,100 without reset
		for (int trials = 1; trials<=100; trials *= 10) {
		tossSimulator.run(trials);
		expectedTrial += trials;
		System.out.println("After run(" + trials + "): ");
		System.out.println("Number of trials [exp:"+ expectedTrial +"]: " + tossSimulator.getNumTrials());
		System.out.println("Two-head tosses: " + tossSimulator.getTwoHeads());
		System.out.println("Two-tail tosses: " + tossSimulator.getTwoTails());
		System.out.println("One-head one-tail tosses: " + tossSimulator.getHeadTails());
		System.out.println("Tosses add up correctly? "
				+ (tossSimulator.getNumTrials() == tossSimulator.getTwoHeads() + tossSimulator.getTwoTails() + tossSimulator.getHeadTails())
				+ "\n");
		}

		//TestCase 5 - Testing the reset function
		tossSimulator.reset();
		System.out.println("After reset:");
		System.out.println("Number of trials [exp:0]: " + tossSimulator.getNumTrials());
		System.out.println("Two-head tosses: " + tossSimulator.getTwoHeads());
		System.out.println("Two-tail tosses: " + tossSimulator.getTwoTails());
		System.out.println("One-head one-tail tosses: " + tossSimulator.getHeadTails());
		System.out.println("Tosses add up correctly? "
				+ (tossSimulator.getNumTrials() == tossSimulator.getTwoHeads() + tossSimulator.getTwoTails() + tossSimulator.getHeadTails())
				+ "\n");
		tossSimulator.reset();

		//TestCase 6 - Testing run for large inputs
		tossSimulator.run(1000);
		System.out.println("After run(1000): ");
		System.out.println("Number of trials [exp:1000]: " + tossSimulator.getNumTrials());
		System.out.println("Two-head tosses: " + tossSimulator.getTwoHeads());
		System.out.println("Two-tail tosses: " + tossSimulator.getTwoTails());
		System.out.println("One-head one-tail tosses: " + tossSimulator.getHeadTails());
		System.out.println("Tosses add up correctly? "
				+ (tossSimulator.getNumTrials() == tossSimulator.getTwoHeads() + tossSimulator.getTwoTails() + tossSimulator.getHeadTails())
				+ "\n");
		tossSimulator.reset();
	}
}
