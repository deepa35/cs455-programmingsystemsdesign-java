//Name: Deepa Sreekumar
//USC NetID: dsreekum
//CS 455 PA1
//Spring 2018

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;

/**
 * Bar class A labeled bar that can serve as a single bar in a bar graph. The
 * text for the label is centered under the bar.
 */
public class Bar {
	private static final Color COLOR_TEXT = Color.BLACK;
	
	private int bottom;
	private int left;
	private int width;
	private int barHeight;
	private double scale;
	private Color color;
	private String label;
	
	
	/**
	 * Creates a labeled bar. You give the height of the bar in application
	 * units (e.g., population of a particular state), and then a scale for how
	 * tall to display it on the screen (parameter scale).
	 * 
	 * @param bottom location of the bottom of the label
	 * @param left location of the left side of the bar
	 * @param width width of the bar (in pixels)
	 * @param barHeight height of the bar in application units
	 * @param scale how many pixels per application unit
	 * @param color the color of the bar
	 * @param label the label at the bottom of the bar
	 */
	public Bar(int bottom, int left, int width, int barHeight, double scale, Color color, String label) {
		this.bottom = bottom;
		this.left = left;
		this.width = width;
		this.barHeight = barHeight;
		this.scale = scale;
		this.color = color;
		this.label = label;
	}

	/**
	 * Draw the labeled bar.
	 * 
	 * @param g2 the graphics context
	 */
	public void draw(Graphics2D g2) {
		// Labeling
		g2.setColor(COLOR_TEXT);
		Font font = g2.getFont();
		FontRenderContext context = g2.getFontRenderContext();
		Rectangle2D labelBounds = font.getStringBounds(label, context);
		int heightOfLabel = (int) labelBounds.getHeight();
		int widthOfLabel = (int) labelBounds.getWidth();
		
		// label = label name; x = x of associated bar - part of label outside of bar area in the left; y = frameheight - bottom gap
		g2.drawString(label, left - Math.round((widthOfLabel- width)/2.0),  bottom);
		
		/* 
		 * Drawing the bar and filling the color
		 * x = left; y = frameHeight - (barheight + labelheight + bottom space)
		 * height of rectangle is barHeight X scaling used
		 */
		if ((int) Math.round(barHeight * scale) != 0) {
			Rectangle bar = new Rectangle(left, bottom - ((int) Math.round(barHeight * scale) + heightOfLabel), width,
					(int) Math.round(barHeight * scale));
			g2.setColor(color);
			g2.draw(bar);
			g2.fill(bar);
		}
	}
}
