//Name: Deepa Sreekumar
//USC NetID: dsreekum
//CS 455 PA1
//Spring 2018

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;

/**
 * class CoinSimComponent
 * 
 * Extends the JComponent class. Overrides paintComponent to draw the bar graph,
 * using Bar objects for each bar in the graph
 */
public class CoinSimComponent extends JComponent {
	private static final int BAR_WIDTH = 80;
	private static final int BOTTOM_TOP_HEIGHT = 50;

	private static final Color COLOR_TWO_HEAD = Color.RED;
	private static final Color COLOR_TWO_TAIL = Color.BLUE;
	private static final Color COLOR_TAIL_HEAD = Color.GREEN;
	private static final String DUMMY_LABEL = "Label";

	private int numTrials;
	private int numTwoHeads;
	private int numTwoTails;
	private int numHeadTail;

	/**
	 * Creates a coin simulator component class with details of simulation.
	 * 
	 * @param trials number of trials to be executed for simulation.
	 */
	public CoinSimComponent(int trials) {
		numTrials = trials;

		CoinTossSimulator tossSimulator = new CoinTossSimulator();
		tossSimulator.run(numTrials);

		numTwoHeads = tossSimulator.getTwoHeads();
		numTwoTails = tossSimulator.getTwoTails();
		numHeadTail = tossSimulator.getHeadTails();
	}

	/**
	 * Creates a paint component function that sketches the graph using Bar
	 * objects.
	 * 
	 * @param g graphics context.
	 * 
	 * NOTE: This function will be called again when a window resize operation occurs.
	 */
	public void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;

		Font font = g2D.getFont();
		FontRenderContext context = g2D.getFontRenderContext();
		Rectangle2D labelBounds = font.getStringBounds(DUMMY_LABEL, context);

		/*
		 * Using dummy label to get the label height for scale calculation
		 */
		int heightOfLabel = (int) labelBounds.getHeight();

		/*
		 * scale is number of pixels per application units. 
		 * BOTTOM_TOP_HEIGHT is the buffer space at top and bottom of frame.
		 */
		double scale = (getHeight() - (2 * BOTTOM_TOP_HEIGHT) - heightOfLabel) / (double) numTrials;
		int left = (int) Math.round((getWidth() - (3 * BAR_WIDTH)) / 4.0); // Frame width minus
														// width of three bars
		int bottom = getHeight() - BOTTOM_TOP_HEIGHT;

		// Creating the bars
		Bar barTwoHeads = new Bar(bottom, left, BAR_WIDTH, numTwoHeads, scale, COLOR_TWO_HEAD,
				"Two Heads: " + numTwoHeads + " (" + Math.round(numTwoHeads / (double) numTrials * 100) + "%)");
		Bar barHeadTail = new Bar(bottom, left * 2 + BAR_WIDTH, BAR_WIDTH, numHeadTail, scale, COLOR_TAIL_HEAD,
				"A head and a Tail: " + numHeadTail + " (" + Math.round(numHeadTail / (double) numTrials * 100) + "%)");
		Bar barTwoTails = new Bar(bottom, left * 3 + BAR_WIDTH * 2, BAR_WIDTH, numTwoTails, scale, COLOR_TWO_TAIL,
				"Two Tails: " + numTwoTails + " (" + Math.round(numTwoTails / (double) numTrials * 100) + "%)");

		// Drawing the bars
		barTwoHeads.draw(g2D);
		barHeadTail.draw(g2D);
		barTwoTails.draw(g2D);
	}
}
