//Name: Deepa Sreekumar
//USC NetID: dsreekum
//CS 455 PA1
//Spring 2018

import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JFrame;

/**
 * class CoinSimViewer
 * 
 * Prompts for the number of trials and creates a JFrame window containing graphical representation of the trial outcomes along with the count and percentage.
 */
public class CoinSimViewer {
	private static final int WINDOW_HEIGHT = 500;
	private static final int WINDOW_WIDTH = 800;

	public static void main(String[] args) {
		try {
			Scanner in = new Scanner(System.in);
			int trials;
			do {
				System.out.print("Enter the number of trials: ");
				trials = in.nextInt();
				if (trials <= 0) {
					System.out.println("ERROR: Number entered must be greater than 0.");
				}
			} while (trials <= 0);
			in.close();

			JFrame window = new JFrame();
			window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
			window.setTitle("CoinSim");
			window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			CoinSimComponent component = new CoinSimComponent(trials);
			window.add(component);
			window.setVisible(true);
		} catch (InputMismatchException e) {
			System.out.println("ERROR: Input format not matching. Please enter an integer with value less than the maximum limit.");
		}
	}
}
